package com.rasputina.layoutexample

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_content_details.*

class ContentDetailsActivity : AppCompatActivity() {

    companion object {
        fun newIntent(context: Context) = Intent(context, ContentDetailsActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content_details)
        fab.setOnClickListener { _ ->
            makeToast(getString(R.string.stub_finish))
        }
    }

    private fun makeToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

}
