package com.rasputina.layoutexample

import android.content.Context
import android.util.AttributeSet
import android.view.View

class CustomView : View {

    companion object {
        const val DEFAULT_ANIMATION_TIME = 300
    }

    private lateinit var title: String
    private var animationTime: Int = DEFAULT_ANIMATION_TIME

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.CustomView, defStyleAttr, defStyleRes)
            title = resources.getString(typedArray.getResourceId(R.styleable.CustomView_title, R.string.title_default))
            animationTime = typedArray.getInt(R.styleable.CustomView_animation_time, DEFAULT_ANIMATION_TIME)
            typedArray.recycle()
        }
    }

}
