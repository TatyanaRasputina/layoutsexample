package com.rasputina.layoutexample

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context) = Intent(context, SettingsActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        version_view.text = Integer.valueOf(BuildConfig.VERSION_CODE).toString()
        volume_chooser.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                // skip
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                // skip
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                filter_volume_value_view.text = Integer.valueOf(progress).toString()
            }
        })

        filter_vibration_switch.setOnCheckedChangeListener { _, isChecked -> showToast("Vibration setting changed: $isChecked") }
        filter_do_not_disturb_switch.setOnCheckedChangeListener { _, isChecked -> showToast("Do not disturb setting changed: $isChecked") }

        faq_view.setOnClickListener { view ->
            //            startActivity(ContentDetailsActivity.newIntent(this))
            Snackbar.make(view, "Current view state: $viewState", Snackbar.LENGTH_SHORT).show()
        }
    }

    private val viewState: SettingsViewState
        get() = SettingsViewState(volume_chooser.progress, filter_vibration_switch.isChecked,
                filter_do_not_disturb_switch.isChecked, version_view.text.toString())

    /*private val messageCrash: String = "First state: volume: ${volume_chooser.progress}, " +
            "vibration enabled: ${filter_vibration_switch.isChecked}, " +
            "do not disturb: ${filter_do_not_disturb_switch.isChecked}, " +
            "version: ${version_view.text}"*/

    private val messageImmutable: String by lazy {
        "First state: volume: ${volume_chooser.progress}, " +
                "vibration enabled: ${filter_vibration_switch.isChecked}, " +
                "do not disturb: ${filter_do_not_disturb_switch.isChecked}, " +
                "version: ${version_view.text}"
    }

    private val message: String
        get() = "Current state: volume: ${volume_chooser.progress}, " +
                "vibration enabled: ${filter_vibration_switch.isChecked}, " +
                "do not disturb: ${filter_do_not_disturb_switch.isChecked}, " +
                "version: ${version_view.text}"

}
