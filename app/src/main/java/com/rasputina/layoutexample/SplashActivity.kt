package com.rasputina.layoutexample

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import java.util.concurrent.TimeUnit

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            startActivity(LoginActivity.newIntent(this))
            finish()
        }, TimeUnit.SECONDS.toMillis(3))
    }

}
