package com.rasputina.layoutexample

data class SettingsViewState(val volume: Int,
                             val isVibration: Boolean,
                             val isDoNotDisturb: Boolean,
                             val version: String)