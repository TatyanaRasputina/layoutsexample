package com.rasputina.layoutexample

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.EditorInfo
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context) = Intent(context, LoginActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        login_button.setOnClickListener { _ ->
            doLogin()
        }

        login_email_view.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                s?.takeIf { it.isNotBlank() }?.let { showToast(it.toString()) }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //do nothing
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                //do nothing
            }
        })

        login_password_view.setOnEditorActionListener { _, actionId, _ ->
            when(actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    doLogin()
                    true
                }
                else -> false
            }
        }
    }

    private fun doLogin() {
        val isValid: Boolean = !login_email_view.text.isEmpty() && !login_password_view.text.isEmpty()
        if (isValid) {
            startActivity(SettingsActivity.newIntent(this))
            return
        }
        showToast(getString(R.string.login_unsuccessful_stub))
    }

}
